/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 19:08:27 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/18 19:33:16 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_power(int nb, int power)
{
	int		i;

	i = power;
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	if (i > 0)
		return (nb * ft_recursive_power(nb, power - 1));
	return (0);
}
