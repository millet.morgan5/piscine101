/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 19:09:27 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/18 19:32:05 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int		i;
	int		result;

	i = 0;
	result = 0;
	while (result != nb)
	{
		result = i * i;
		if (result == nb)
			break ;
		else if (result > nb)
			return (0);
		else
			i++;
	}
	return (i);
}
