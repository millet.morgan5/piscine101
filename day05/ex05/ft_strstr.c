/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 14:22:41 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/19 09:55:50 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	while (str[i] != '\0')
	{
		if (to_find[len] == str[i])
		{
			while (to_find[len] == str[i + len])
				len++;
		}
		if (to_find[len] == '\0')
			return (str + i);
		else
			len = 0;
		i++;
	}
	return (0);
}
