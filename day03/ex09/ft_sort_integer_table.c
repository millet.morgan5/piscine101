/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/11 15:36:58 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/14 19:08:32 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_integer_table(int *tab, int size)
{
	int i;
	int tmp;
	int j;

	i = size;
	j = 0;
	tmp = 0;
	while (i > 0)
	{
		while (j < (i - 1))
		{
			if (tab[j] > tab[j + 1])
			{
				tmp = tab[j + 1];
				tab[j + 1] = tab[j];
				tab[j] = tmp;
				j++;
			}
			else
				j++;
		}
		j = 0;
		i--;
	}
}
