/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/11 09:55:07 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/14 13:29:00 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		mm_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

char	*ft_strrev(char *str)
{
	int		len;
	int		a;
	char	tmp;

	len = mm_strlen(str) - 1;
	a = 0;
	while (a <= len / 2)
	{
		tmp = str[a];
		str[a] = str[len - a];
		str[len - a] = tmp;
		a++;
	}
	return (str);
}
