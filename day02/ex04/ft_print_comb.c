/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 08:37:32 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/10 17:47:53 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_putchar_result(char number1, char number2, char number3)
{
	ft_putchar(number1);
	ft_putchar(number2);
	ft_putchar(number3);
}

void	ft_print_comb(void)
{
	int number1;
	int number2;
	int number3;

	number1 = '0';
	while (number1 <= '7')
	{
		number2 = number1 + 1;
		while (number2 <= '8')
		{
			number3 = number2 + 1;
			while (number3 <= '9')
			{
				ft_putchar_result(number1, number2, number3);
				if (number1 != '7')
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
				number3++;
			}
			number2++;
		}
		number1++;
	}
}
