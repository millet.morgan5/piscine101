/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 20:11:51 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/13 14:30:33 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_putchar_result(int a, int b, int c, int d)
{
	ft_putchar(a + '0');
	ft_putchar(b + '0');
	ft_putchar(' ');
	ft_putchar(c + '0');
	ft_putchar(d + '0');
	if (!(a == 9 && b == 8))
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_2(int a, int b, int c, int d)
{
	while (c <= 9)
	{
		while (d <= 9)
		{
			ft_putchar_result(a, b, c, d);
			d++;
		}
		d = 0;
		c++;
	}
}

void	ft_print_comb2(void)
{
	int a;
	int b;
	int c;
	int d;

	a = 0;
	b = 0;
	c = 0;
	d = 1;
	while (a <= 9)
	{
		while (b <= 9)
		{
			ft_print_2(a, b, c, d);
			b++;
			d = b + 1;
		}
		if (b > 9)
		{
			a++;
			b = 0;
			c = a;
			d = b + 1;
		}
	}
}
