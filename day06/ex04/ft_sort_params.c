/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmillet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 09:59:20 by mmillet           #+#    #+#             */
/*   Updated: 2017/09/21 13:53:53 by mmillet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] && s2[i] && s1[i] == s2[i])
		i++;
	if (s1[i] != s2[i])
		return (s1[i] - s2[i]);
	else
		return (0);
}

char	**ft_sort_integer_table(int argc, char **argv)
{
	int		i;
	int		j;
	char	*tri;

	i = 0;
	j = 1;
	while (i < argc)
	{
		while (j < argc - 1)
		{
			if (ft_strcmp(argv[j], argv[j + 1]) > 0)
			{
				tri = argv[j];
				argv[j] = argv[j + 1];
				argv[j + 1] = tri;
			}
			j++;
		}
		i++;
		j = 0;
	}
	return (argv);
}

int		main(int argc, char **argv)
{
	int i;

	i = 1;
	if (argc == -1)
		return (-1);
	ft_sort_integer_table(argc, argv);
	while (i < argc)
	{
		ft_putstr(argv[i]);
		ft_putchar('\n');
		i++;
	}
	return (0);
}
